package org.jxls.expression;

/**
 * An interface to evaluate expressions
 * Date: Nov 2, 2009
 * @author Leonid Vysochyn
 */
public interface ExpressionEvaluator {
    Object evaluate(String expression);
}
